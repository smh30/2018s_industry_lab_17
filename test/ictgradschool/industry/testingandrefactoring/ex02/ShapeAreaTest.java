package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ShapeAreaTest {

    private ShapeAreaCalculator myCalc;

    @Before
    public void setUp(){
        myCalc = new ShapeAreaCalculator();
    }

// this method should take a string and convert it to a double
    @Test
    public void convertStringTest(){
        double converted = 0;
        try {
            converted = myCalc.convertString("23.498");
        } catch (NumberFormatException e){
            fail();
        }
        assertEquals(23.498, converted, 1e-15);
    }

    @Test
    public void convertUnconvertableTest(){
        double converted = 0;
        try {
            converted = myCalc.convertString("hello");
            fail();
        } catch (NumberFormatException e){
            assertEquals(0, converted,  1e-15);
        }
    }

    @Test
    public void rectangleAreaTest(){
        double width = 5.10;
        double height = 8.25;

        double area = myCalc.rectangleArea(width, height);
        assertEquals(42.075, area, 1e-10);
    }


    // not sure about this one as the actual value from calculator is much longer, maybe delta need to change?
    // reduced delta so that the result doesn't have to be so close
    @Test
    public void circleAreaTest(){
        int radius = 2;

        double area = myCalc.circleArea(radius);
        assertEquals(12.566370614, area, 1e-8);
    }

    @Test
    public void roundingDownTest(){
        assertEquals(2, myCalc.round(2.478));
    }

    @Test
    public void roundingUpTest(){
        assertEquals(4, myCalc.round(3.78));
    }

    @Test
    public void comparisonTestSmallFirst(){
        assertEquals(myCalc.compare(234, 968), 234);
    }

    @Test
    public void comparisonTestBigFirst(){
        assertEquals(myCalc.compare(690, 689), 689);
    }
    
    
}
