package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testTurnEast() {
        int initRow = myRobot.row();
        int initCol = myRobot.column();
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(initRow, myRobot.row());
        assertEquals(initCol, myRobot.column());
    }

    @Test
    public void testTurnSouth() {
        int initRow = myRobot.row();
        int initCol = myRobot.column();
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(initRow, myRobot.row());
        assertEquals(initCol, myRobot.column());
    }

    @Test
    public void testTurnWest() {
        int initRow = myRobot.row();
        int initCol = myRobot.column();
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(initRow, myRobot.row());
        assertEquals(initCol, myRobot.column());
    }

    @Test
    public void testTurnNorth() {
        int initRow = myRobot.row();
        int initCol = myRobot.column();
        assertEquals(Robot.Direction.North, myRobot.getDirection());

        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(initRow, myRobot.row());
        assertEquals(initCol, myRobot.column());
    }

    @Test
    public void testValidMoveNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        try {
            myRobot.move();
            assertEquals(myRobot.row(), 9);
            assertEquals(myRobot.column(), 1);
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testValidMoveEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();
        try {
            myRobot.move();
            assertEquals(myRobot.column(), 2);
            assertEquals(myRobot.row(), 10);
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testValidMoveSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        try {
            myRobot.move();
            assertEquals(myRobot.row(), 9);
        } catch (IllegalMoveException e) {
            fail("moving north to get away from edge failed");
        }
        //turn twice to be facing south
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
            assertEquals(myRobot.column(), 1);
            assertEquals(myRobot.row(), 10);
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testValidMoveWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();
        try {
            myRobot.move();
            assertEquals(myRobot.column(), 2);
        } catch (IllegalMoveException e) {
            fail("moving east to get away from edge failed");
        }
        //turn twice to be facing south
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
            assertEquals(myRobot.column(), 1);
            assertEquals(myRobot.row(), 10);
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testIllegalMoveNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        boolean atRight = false;
        myRobot.turn();
        try {
            // Move the robot to the right row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.column());
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        boolean atBottom = false;
        assertEquals(myRobot.currentState().row, Robot.GRID_SIZE);
        atBottom = true;
        myRobot.turn();
        myRobot.turn();

        try {
            // Now try to continue to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.row());
        }
    }

    @Test
    public void testIllegalMoveWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        boolean atLeft = false;
        assertEquals(myRobot.currentState().column, 1);
        atLeft = true;
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();

        try {
            // Now try to continue to move Left.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.column());
        }
    }

    // is this cheating? it wouldn't work if the states list was private
    @Test
    public void testBacktrackListSize() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        // turn to face east, and do some moves so there's something to backtrack
        myRobot.turn();
        try {
            myRobot.move();
            myRobot.move();
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(myRobot.column(), 4);
        int originalSize = myRobot.states.size();

        myRobot.backTrack();
        assertEquals(originalSize - 1, myRobot.states.size());
    }

    @Test
    public void testBacktrack() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        int initRow = myRobot.row();
        int initCol = myRobot.column();
        // turn to face east, and do a move so there's something to backtrack
        myRobot.turn();
        try {
            myRobot.move();
            assertEquals(myRobot.column(), 2);
            assertEquals(myRobot.row(), 10);
        } catch (IllegalMoveException e) {
            fail();
        }

        myRobot.backTrack();
        assertEquals(initRow, myRobot.row());
        assertEquals(initCol, myRobot.column());

    }


}
