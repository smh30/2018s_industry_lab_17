package ictgradschool.industry.testingandrefactoring.ex03;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class ExcelTest {
    ExcelNew excel;
    @Before
    public void setUp(){
        excel = new ExcelNew();
    }

    @Test
    public void testRandom(){
        for(int i=0; i< 100; i++) {
            int random = excel.getRandomMark(20, 60);
            assertTrue(random >= 20 && random < 60);
        }
    }
}
