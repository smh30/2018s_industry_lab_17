package ictgradschool.industry.testingandrefactoring.ex03;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {
    String fileLine;
    String output = "";
    int classSize = 550;
    ArrayList<String> firstNameList;
    ArrayList<String> surnameList;
    
    
    public static void main(String[] args) {
        ExcelNew excel = new ExcelNew();
        excel.start();
    }
    
    
    public ExcelNew() {
        firstNameList = new ArrayList<String>();
        surnameList = new ArrayList<String>();
    }
    
    public void start() {
        readFirstNames();
        readSurnames();
        
        for (int i = 1; i <= classSize; i++) {
            String student = createStudentString(i);
            output += student;
        }
        writeFile();
    }
    
    
    private String createStudentString(int i) {
        String studentLine = "";
        studentLine += String.format("%04d", i);
        
        String surname = surnameList.get((int) (Math.random() * surnameList.size()));
        String firstname = firstNameList.get((int) (Math.random() * firstNameList.size()));
        studentLine += "\t" + surname + "\t" + firstname + "\t";
        
        int randStudentSkill = (int) (Math.random() * 101);
        
        int numLabs = 3;
        
        for (int j = 0; j < numLabs; j++) {
            studentLine += randomiseLabMarks(randStudentSkill);
            studentLine += "\t";
        }
        
        studentLine += randomiseTestMark(randStudentSkill);
        studentLine += "\t";
        
        studentLine += randomiseExamMark(randStudentSkill);
        
        studentLine += "\n";
        return studentLine;
    }
    
    private String randomiseExamMark(int randStudentSkill) {
        String examMark = "";
        if (randStudentSkill <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                examMark += "";
            } else {
                examMark += (int) (Math.random() * 40);
            }
        } else if ((randStudentSkill > 7) && (randStudentSkill <= 20)) {
            examMark += getRandomMark(40, 50);
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 60)) {
            examMark += getRandomMark(50, 70);
        } else if ((randStudentSkill > 60) && (randStudentSkill <= 90)) {
            examMark += getRandomMark(70, 90);
        } else {
            examMark += getRandomMark(90, 100);
        }
        return examMark;
    }
    
    
    private String randomiseTestMark(int randStudentSkill) {
        String testMark = "";
        if (randStudentSkill <= 5) {
            testMark += (int) (Math.random() * 40);
        } else if ((randStudentSkill > 5) && (randStudentSkill <= 20)) {
            testMark += getRandomMark(40, 50);
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 65)) {
            testMark += getRandomMark(50, 70);
        } else if ((randStudentSkill > 65) && (randStudentSkill <= 90)) {
            testMark += getRandomMark(70, 90);
        } else {
            testMark += getRandomMark(90, 100);
        }
        return testMark;
    }
    
    private String randomiseLabMarks(int randStudentSkill) {
        String labMarks = "";
        if (randStudentSkill <= 5) {
            labMarks += (int) (Math.random() * 40);
        } else if ((randStudentSkill > 5) && (randStudentSkill <= 15)) {
            labMarks += getRandomMark(40, 50);
        } else if ((randStudentSkill > 15) && (randStudentSkill <= 25)) {
            labMarks += getRandomMark(50, 70);
        } else if ((randStudentSkill > 25) && (randStudentSkill <= 65)) {
            labMarks += getRandomMark(70, 90);
        } else {
            labMarks += getRandomMark(90, 100);
        }
        return labMarks;
    }
    
    public int getRandomMark(int lower, int upper) {
        return (int) (Math.random() * (upper - lower) + lower);
    }
    
    private void writeFile() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
            bw.write(output);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void readSurnames() {
        try {
            BufferedReader br;
            br = new BufferedReader(new FileReader("Surnames.txt"));
            while ((fileLine = br.readLine()) != null) {
                surnameList.add(fileLine);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void readFirstNames() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("FirstNames.txt"));
            while ((fileLine = br.readLine()) != null) {
                firstNameList.add(fileLine);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}