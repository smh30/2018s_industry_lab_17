package ictgradschool.industry.testingandrefactoring.ex02;

import ictgradschool.Keyboard;

public class ShapeAreaCalculator {
    public static void main(String[] args) {
        ShapeAreaCalculator s = new ShapeAreaCalculator();
        s.start();
    }

    public ShapeAreaCalculator(){
    }

    public void start(){
        System.out.println("Welcome to Shape Area Calculator!\n");
        String widthString = getClientWidth();
        String heightString = getClientHeight();
        double width = convertString(widthString);
        double height = convertString(heightString);
        double rectArea = rectangleArea(width, height);
        int radius = (int)(Math.random()*50 +1);
        System.out.println("The radius of the circle is: " + radius);
        double circleArea = circleArea(radius);
        System.out.println("The smaller area is: " + compare(round(rectArea), round(circleArea)));
    }

    public String getClientWidth(){
        System.out.println("Enter the width of the rectangle: ");
        return Keyboard.readInput();
    }

    public String getClientHeight(){
        System.out.println("Enter the height of the rectangle: ");
        return Keyboard.readInput();
    }

    public double convertString(String string) {
        double result = Double.parseDouble(string);
        return result;
    }

    public double rectangleArea(double width, double height){
        double result = width * height;
        return result;
    }

    public double circleArea(int radius){
        double result = Math.PI * radius * radius;
        return result;
    }

    public int round(double dub){
        int result = (int)Math.round(dub);
        return result;
    }

    //compares and returns the smaller
    public int compare(int a, int b){
        int result = Math.min(a, b);
        return result;
    }
}