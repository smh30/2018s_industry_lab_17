package ictgradschool.industry.testingandrefactoring.ex04;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import static java.awt.event.KeyEvent.*;

/**
 * TODO Have fun :)
 */
@SuppressWarnings("Duplicates")
public class SnakeGame extends JFrame {


    final int GRIDWIDTH = 25;
    final int BOXSIZE = 23;
    final int WIDTH_IN_GRIDUNITS = 30;
    final int HEIGHT_IN_GRIDUNITS = 20;
    final int canvasWidth = WIDTH_IN_GRIDUNITS * GRIDWIDTH + 6;
    final int canvasHeight = HEIGHT_IN_GRIDUNITS * GRIDWIDTH + 28;

    private static final Random r = new Random();
    private Canvas c = new Canvas();
    private int greenX = -1, greenY = -1;
    private ArrayList<Integer> redXs = new ArrayList<>();
    private ArrayList<Integer> snakeXs = new ArrayList<>();
    private ArrayList<Integer> redYs = new ArrayList<>();
    private ArrayList<Integer> snakeYs = new ArrayList<>();
    private boolean done = false;
    private Directions direction;


    public static void main(String[] args) {
        new SnakeGame().go();
    }
    
    public SnakeGame() {
// add the initial 5 blocks to form the snake
        for (int i = 10; i > 4; i--) {
            snakeXs.add(i);
            snakeYs.add(10);
        }
        
        this.direction = Directions.RIGHT;
        setTitle("SnakeGame : 6");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 756, 528);
        setResizable(false);
        c.setBackground(Color.white);
        add(BorderLayout.CENTER, c);
        
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case VK_UP:
                        if (direction != Directions.DOWN) {
                            direction = Directions.UP;
                        }
                        break;
                    case VK_DOWN:
                        if (direction != Directions.UP) {
                            direction = Directions.DOWN;
                        }
                        break;
                    case VK_LEFT:
                        if (direction != Directions.RIGHT) {
                            direction = Directions.LEFT;
                        }
                        break;
                    case VK_RIGHT:
                        if (direction != Directions.LEFT) {
                            direction = Directions.RIGHT;
                        }
                        break;
                }
                
                
            }
        });
        setVisible(true);
    }
    
    void go() { // main loop
        while (!done) {
            int xFrontOfSnake = snakeXs.get(0);
            int yFrontOfSnake = snakeYs.get(0);
            if (direction == Directions.LEFT) {
                xFrontOfSnake--;
            }
            if (direction == Directions.RIGHT) {
                xFrontOfSnake++;
            }
            if (direction == Directions.UP) {
                yFrontOfSnake--;
            }
            if (direction == Directions.DOWN) {
                yFrontOfSnake++;
            }

            // if it goes off the screen, wrap it around
            if (xFrontOfSnake > WIDTH_IN_GRIDUNITS - 1) {
                xFrontOfSnake = 0;
            }
            if (xFrontOfSnake < 0) {
                xFrontOfSnake = WIDTH_IN_GRIDUNITS - 1;
            }
            if (yFrontOfSnake > HEIGHT_IN_GRIDUNITS - 1) {
                yFrontOfSnake = 0;
            }
            if (yFrontOfSnake < 0) {
                yFrontOfSnake = HEIGHT_IN_GRIDUNITS - 1;
            }

            done = !checkPointFree(xFrontOfSnake, yFrontOfSnake);
            snakeXs.add(0, xFrontOfSnake);
            snakeYs.add(0, yFrontOfSnake);
            
            // if you eat a green box......
            if (((snakeXs.get(0) == greenX) && (snakeYs.get(0) == greenY))) {
                // take away the green box
                greenX = -1;
                greenY = -1;
                setTitle("SnakeGame" + " : " + snakeXs.size());
            } else {
                //if you didn't eat a green, remove the last box of the snake (as part of the 'movement')
                snakeXs.remove(snakeXs.size() - 1);
                snakeYs.remove(snakeYs.size() - 1);
            }
            
            // if there are no green boxes onscreen, create one
            if (greenX == -1) {
                Point newGreen = getNewBoxPoint();
                greenX = (int) newGreen.getX();
                greenY = (int) newGreen.getY();

                Point newRed = getNewBoxPoint();
                redXs.add((int) newRed.getX());
                redYs.add((int) newRed.getY());
            }
            c.repaint();
            try {
                // does this sleep rather than using a timer to run the thing??????? weird but works i guess
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < snakeXs.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeXs.get(i) * GRIDWIDTH + 1, snakeYs.get(i) * GRIDWIDTH + 1, BOXSIZE,  BOXSIZE, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(greenX * GRIDWIDTH + 1, greenY * GRIDWIDTH + 1, BOXSIZE, BOXSIZE, true);
            for (int i = 0; i < redXs.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(redXs.get(i) * GRIDWIDTH + 1, redYs.get(i) * GRIDWIDTH + 1, BOXSIZE, BOXSIZE, true);
            }
            if (done) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (canvasWidth - fm.stringWidth("Over")) / 2, (canvasHeight) / 2);
            }
        }
    }
    
    private Point getNewBoxPoint() {
        int x, y;
        do {
            x = r.nextInt(30);
            y = r.nextInt(20);

        } while (!checkPointFree(x, y) || greenX == x && greenY == y);
        
        return new Point(x, y);
    }

    private boolean checkPointFree(int x, int y){
        boolean free = true;
        for (int i = 0; i < redXs.size(); i++) {
            if (redXs.get(i) == x && redYs.get(i) == y) {
                return false;
            }
        }
        for (int i = 0; i < snakeXs.size(); i++) {
            if ((snakeXs.get(i) == x) && (snakeYs.get(i) == y)) {
                if (!((snakeXs.get(snakeXs.size() - 1) == x) && (snakeYs.get(snakeYs.size() - 1) == y))) {
                    return false;
                }
            }
        }
        return free;
    }

}