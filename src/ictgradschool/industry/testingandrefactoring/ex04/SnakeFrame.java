package ictgradschool.industry.testingandrefactoring.ex04;

import javax.swing.*;
import java.awt.*;

public class SnakeFrame extends JFrame {

    public SnakeFrame(String title, int x, int y, int width, int height){
        setTitle(title);
        setBounds(x, y, width, height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SnakePanel gamePanel = new SnakePanel(this);
        gamePanel.setPreferredSize(new Dimension(width, height));
        Container visibleArea = getContentPane();
        visibleArea.add(gamePanel);
        pack();
        gamePanel.go();

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SnakeFrame frame = new SnakeFrame("Snake : 6", 200, 200, 756, 528);
                frame.setVisible(true);
            }
        });
    }
}
