package ictgradschool.industry.testingandrefactoring.ex04;

public enum Directions {
    UP, DOWN, LEFT, RIGHT, NONE;
}
